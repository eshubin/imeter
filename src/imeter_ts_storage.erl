-module(imeter_ts_storage).

%% API
-export([behaviour_info/1, from_list/2]).

behaviour_info(callbacks) ->
    [
        {new, 0},
        {append, 2},
        {len, 1},
        {lower_bound, 2},
        {start_ts, 1},
        {delete, 1}
    ].

from_list(Backend, L) ->
    lists:foldl(
        fun Backend:append/2,
        Backend:new(),
        L
    ).

