-module(imeter_ts_ets).
-behaviour(imeter_ts_storage).

%% API
-export([
    new/0,
    append/2,
    len/1,
    lower_bound/2,
    start_ts/1,
    delete/1
]).

-include_lib("stdlib/include/ms_transform.hrl").

new() ->
    ets:new(timestamps, [ordered_set, private]).

start_ts(Storage) ->
    case ets:first(Storage) of
        '$end_of_table' ->
            empty;
        TS ->
            {value, TS}
    end.

len(Storage) ->
    ets:foldl(
        fun({_, Cnt}, Acc) ->
            Acc + Cnt
        end,
        0,
        Storage
    ).

append(TS, Storage) ->
    case ets:insert_new(Storage, {TS, 1}) of
        true -> ok;
        false ->
            ets:update_counter(Storage, TS, 1)
    end,
    Storage.

lower_bound(TS, Storage) ->
    MS = ets:fun2ms(fun({K, _}) when K<TS -> true end),
    ets:select_delete(Storage, MS),
    Storage.

delete(Storage) ->
    ets:delete(Storage).