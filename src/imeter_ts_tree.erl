-module(imeter_ts_tree).

-behaviour(imeter_ts_storage).

%% API
-export([
    new/0,
    append/2,
    len/1,
    lower_bound/2,
    start_ts/1,
    delete/1
]).

new() ->
    gb_trees:empty().

append(TS, Storage) ->
    case gb_trees:lookup(TS, Storage) of
        none ->
            gb_trees:enter(TS, 1, Storage);
        {value, V} ->
            gb_trees:update(TS, V+1, Storage)
    end.

len(Storage) ->
    lists:sum(gb_trees:values(Storage)).

lower_bound(TS, Storage) ->
    gb_trees_ext:foldl(
        fun({Key, Val}, Acc) ->
            gb_trees:enter(Key, Val, Acc)
        end,
        gb_trees:empty(),
        gb_trees_ext:lower_bound(TS, Storage)
    ).

start_ts(Storage) ->
    case gb_trees:is_empty(Storage) of
        true ->
            empty;
        false ->
            {TS, _} = gb_trees:smallest(Storage),
            {value, TS}
    end.

delete(_)->
    true.




