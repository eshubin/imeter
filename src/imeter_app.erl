-module(imeter_app).

-behaviour(application).

-include("imeter_settings.hrl").

%% Application callbacks
-export([start/2, stop/1]).

%% ===================================================================
%% Application callbacks
%% ===================================================================

start(_StartType, _StartArgs) ->
    {ok, SendMessageLength} = application:get_env(send_message_length),
    {ok, RecvMessageLength} = application:get_env(receive_message_length),
    {ok, MeasurePeriod} = application:get_env(measure_period),
    {ok, RefreshPeriod} = application:get_env(refresh_period),
    {ok, Port} = application:get_env(listen_port),
    {ok, Storage} = application:get_env(storage),

    Settings = #settings{
        send_message_length = SendMessageLength,
        receive_message_length = RecvMessageLength,
        measure_period = MeasurePeriod,
        refresh_period = RefreshPeriod * 1000, %convert seconds to ms
        storage = Storage
    },

    Dispatch = cowboy_router:compile([
        {'_', [
            {"/bullet", imeter_handler, [{imeter_settings, Settings}]},
            {"/[...]", cowboy_static, {priv_dir, imeter, ""}}
        ]}
    ]),
    {ok, _} = cowboy:start_http(
        http, 100,
        [{port, Port}],
        [{env, [{dispatch, Dispatch}]}]
    ),
    imeter_sup:start_link().

stop(_State) ->
    ok.
