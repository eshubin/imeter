%%%-------------------------------------------------------------------
%%% @author eugene
%%% @copyright (C) 2013, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 10. Dec 2013 6:51 PM
%%%-------------------------------------------------------------------
-module(imeter_time).
-author("eugene").

%% API
-export([add/2]).


add({Mega, Sec, Micro}, Seconds) ->
    {Mega, Sec + Seconds, Micro}.

%% =============================================================================

-include_lib("eunit/include/eunit.hrl").

add_test_() ->
    [
        ?_assertEqual(0, timer:now_diff({1,2,3}, add({1,3,3}, -1))),
        ?_assertEqual(0, timer:now_diff({0,999999,3}, add({1,3,3}, -4))),
        ?_assertEqual(0, timer:now_diff({1,0,3}, add({1,3,3}, -3)))
    ].
