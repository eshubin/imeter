-module(imeter_handler).
-behaviour(cowboy_websocket_handler).

-export([init/3, websocket_init/3, websocket_handle/3,
    websocket_info/3, websocket_terminate/3]).

-include("imeter_settings.hrl").

-record(state,
{
    server_sent_messages,
    server_recv_messages,
    response,    %cache
    settings,
    storage_type
}
).

-define(PROTOCOL_HEADER_LENGTH, 2).


init({tcp, http}, _Req, Opts) ->
    lager:debug("init opts ~p", [Opts]),
    {upgrade, protocol, cowboy_websocket}.


websocket_init(_TransportName, Req, Opts) ->
    lager:debug("websocket_init opts ~p", [Opts]),
    Settings = proplists:get_value(imeter_settings, Opts),
    StorageType = Settings#settings.storage,
    State = update_state_on_refresh(
        #state{
            settings = Settings,
            storage_type = StorageType,
            server_recv_messages = StorageType:new(),
            server_sent_messages = StorageType:new()
        }
    ),
%%     schedule_send(),
    schedule_refresh(Settings#settings.refresh_period),
    {ok, Req, State}. %%, inifinity

websocket_handle(
        {text, Input},
        Req,
        #state{settings = S} = State
) when is_binary(Input) ->
    L = byte_size(Input),
    L = S#settings.receive_message_length - ?PROTOCOL_HEADER_LENGTH,
    {ok, Req, update_state_on_recv(State)}.

websocket_info(send_data, Req, State) ->
    Resp = response(State),
    NewState = update_state_on_send(State),
    schedule_send(),
    {reply, Resp, Req, NewState};
websocket_info(refresh, Req, #state{settings = Settings} = State) ->
    NewState = update_state_on_refresh(State),
    lager:debug(
        "refreshed to ~p", [NewState#state.response]
    ),
    schedule_refresh(Settings#settings.refresh_period),
    {ok, Req, NewState}.

websocket_terminate(_Reason, _Req,
        #state{
            server_recv_messages = RecvMessages,
            server_sent_messages = SentMessages,
            storage_type = StorageType
        }
) ->
    true = StorageType:delete(RecvMessages),
    true = StorageType:delete(SentMessages).

%% =============================================================================

update_state_on_recv(#state{
        server_recv_messages = Counter,
        storage_type = StorageType
    } = State) ->
    State#state{server_recv_messages = append_timestamp(Counter, StorageType)}.

update_state_on_send(#state{
        server_sent_messages = Counter,
        storage_type = StorageType
    } = State) ->
    State#state{server_sent_messages = append_timestamp(Counter, StorageType)}.

update_state_on_refresh(#state{
        server_recv_messages = R,
        server_sent_messages = S,
        settings = Settings,
        storage_type = StorageType
} = State) ->
    End = os:timestamp(),
    LowerLimit = imeter_time:add(End, -Settings#settings.measure_period),
    Received = StorageType:lower_bound(LowerLimit, R),
    Sent = StorageType:lower_bound(LowerLimit, S),
    Response = refresh_response(Received, Sent, End,
        Settings#settings.send_message_length,
        Settings#settings.receive_message_length,
        StorageType
    ),
    State#state{
        server_recv_messages = Received,
        server_sent_messages = Sent,
        response = Response
    }.

append_timestamp(Storage, StorageType) ->
    StorageType:append(os:timestamp(), Storage).

schedule_send() ->
    self() ! send_data.

schedule_refresh(RefreshPeriod) ->
    timer:send_after(RefreshPeriod, refresh).

refresh_response(Received, Sent, End, SendMessageLength,
        RecvMessageLength, StorageType) ->
    {DownloadSpeed, UploadSpeed} = calculate_speed(
        Received, Sent, End, SendMessageLength, RecvMessageLength, StorageType
    ),
    {text, serialize_response(DownloadSpeed, UploadSpeed, SendMessageLength)}.

response(#state{response = Resp}) ->
    Resp.

calculate_speed(Received, Sent, End,
    SendMessageLength, RecvMessageLength, StorageType) ->
    {
        speed(Sent, End, SendMessageLength, StorageType),
        speed(Received, End, RecvMessageLength, StorageType)
    }.

speed(Timestamps, End, MessageLength, StorageType) ->
    case StorageType:start_ts(Timestamps) of
        empty ->
            0;
        {value, Begin} ->
            Period = timer:now_diff(End, Begin) div 1000000,
            speed(
                MessageLength * StorageType:len(Timestamps),
                Period
            )
    end.

speed(_, 0) ->
    0;
speed(Bytes, Period) -> %Bytes per second
    Bytes div Period.

serialize_response(DownloadSpeed, UploadSpeed, SendMessageLength) ->
    NumTextLength = num_text_length(DownloadSpeed) +
        num_text_length(UploadSpeed),
    Padding = get_padding(NumTextLength, SendMessageLength),
    Data = jiffy:encode([DownloadSpeed, UploadSpeed]),
    <<Data/binary, Padding/binary>>.


num_text_length(Num) ->
    length(integer_to_list(Num)).

get_padding(NumTextLength, SendMessageLength) ->
    Length = SendMessageLength - NumTextLength
        - 2 %[]
        - 1 %,
        - ?PROTOCOL_HEADER_LENGTH,
    binary:copy(<<" ">>, Length).


%% =============================================================================

-include_lib("eunit/include/eunit.hrl").
-define(TEST_SEND_MESSAGE_LENGTH, 32).
-define(TEST_RECV_MESSAGE_LENGTH, 32).

update_state_on_send_test() ->
    InitialState = #state{
        server_sent_messages = imeter_ts_queue:new(),
        storage_type = imeter_ts_queue
    },
    NewState = update_state_on_send(InitialState),
    ?assertEqual(
        1,
        imeter_ts_queue:len(NewState#state.server_sent_messages)
    ).

update_state_on_recv_test() ->
    InitialState = #state{
        server_recv_messages = imeter_ts_queue:new(),
        storage_type = imeter_ts_queue
    },
    NewState = update_state_on_recv(InitialState),
    ?assertEqual(
        1,
        imeter_ts_queue:len(NewState#state.server_recv_messages)
    ).


speed_test_() ->
    [
        ?_assertEqual(10, speed(10, 1)),
        ?_assertEqual(0, speed(10, 0))
    ].


expected_response(Resp, SendMessageLength) ->
    Padding = binary:copy(
        <<" ">>,
        SendMessageLength - byte_size(Resp) - ?PROTOCOL_HEADER_LENGTH),
    <<Resp/binary, Padding/binary>>.

serialize_response_test_() ->
    [
        ?_assertEqual(
            expected_response(<<"[1,1]">>, ?TEST_SEND_MESSAGE_LENGTH),
            serialize_response(1, 1, ?TEST_SEND_MESSAGE_LENGTH)
        ),
        ?_assertEqual(
            expected_response(<<"[12345,6789]">>, ?TEST_SEND_MESSAGE_LENGTH),
            serialize_response(12345, 6789, ?TEST_SEND_MESSAGE_LENGTH)
        ),
        ?_assertEqual(
            expected_response(<<"[1111111111111111,22222222222]">>,
                ?TEST_SEND_MESSAGE_LENGTH),
            serialize_response(1111111111111111, 22222222222,
                ?TEST_SEND_MESSAGE_LENGTH)
        ),
        ?_assertError(
            _,
            serialize_response(11111111111111113, 22222222222,
                ?TEST_SEND_MESSAGE_LENGTH)
        ),
        ?_assertError(
            _,
            serialize_response(
                1000000000000000,
                1000000000000000, ?TEST_SEND_MESSAGE_LENGTH)
        )
    ].



calculate_speed_test_()->
    [
        ?_assertEqual(
            {0,32},
            calculate_speed(
                imeter_ts_queue:from_list([{1, 3, 20}]),
                imeter_ts_queue:from_list([{1, 4, 20}]),
                {1, 4, 20}, ?TEST_SEND_MESSAGE_LENGTH,
                ?TEST_RECV_MESSAGE_LENGTH,
                imeter_ts_queue
            )
        ),
        ?_assertEqual(
            {16,64},
            calculate_speed(
                imeter_ts_queue:from_list([{1, 3, 20}, {1, 3, 20}]),
                imeter_ts_queue:from_list([{1, 0, 20}, {1, 2, 20}]),
                {1, 4, 20}, ?TEST_SEND_MESSAGE_LENGTH,
                ?TEST_RECV_MESSAGE_LENGTH,
                imeter_ts_queue
            )
        )
    ].

refresh_response_test_() ->
    [
        ?_assertEqual(
            {text, expected_response(<<"[0,0]">>, ?TEST_SEND_MESSAGE_LENGTH)},
            refresh_response(
                imeter_ts_queue:new(),
                imeter_ts_queue:new(),
                {1, 4, 20}, ?TEST_SEND_MESSAGE_LENGTH,
                ?TEST_RECV_MESSAGE_LENGTH,
                imeter_ts_queue
            )
        ),
        ?_assertEqual(
            {text, expected_response(<<"[0,32]">>, ?TEST_SEND_MESSAGE_LENGTH)},
            refresh_response(
                imeter_ts_queue:from_list([{1, 3, 20}]),
                imeter_ts_queue:from_list([{1, 4, 20}]),
                {1, 4, 20}, ?TEST_SEND_MESSAGE_LENGTH,
                ?TEST_RECV_MESSAGE_LENGTH,
                imeter_ts_queue
            )
        )
    ].

