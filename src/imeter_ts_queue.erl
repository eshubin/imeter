-module(imeter_ts_queue).

-behaviour(imeter_ts_storage).

%% API
-export([
    new/0,
    append/2,
    len/1,
    lower_bound/2,
    start_ts/1,
    from_list/1,
    delete/1
]).

new() ->
    queue:new().

append(TS, Storage) ->
    queue:in(TS, Storage).

len(Storage) ->
    queue:len(Storage).

start_ts(Storage) ->
    queue:peek(Storage).

lower_bound(TS, Storage) ->
    case queue:out(Storage) of
        {empty, _} ->
            Storage;
        {{value, V}, _} when V >= TS ->
            Storage;
        {{value, V}, NewStorage} when V < TS ->
            lower_bound(
                TS,
                NewStorage
            )
    end.

delete(_)->
    true.

from_list(L) ->
    imeter_ts_storage:from_list(?MODULE, L).

