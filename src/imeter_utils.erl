%%%-------------------------------------------------------------------
%%% @author eugene
%%% @copyright (C) 2013, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 04. Dec 2013 11:42 AM
%%%-------------------------------------------------------------------
-module(imeter_utils).

%% API
-export([start_app_with_deps/1]).

start_app_with_deps(Name) ->
    case application:start(Name) of
        {error, {not_started, DepName}} ->
            case start_app_with_deps(DepName) of
                {error, _} = Ret ->
                    Ret;
                _ ->
                    start_app_with_deps(Name)
            end;
        OtherOutput ->
            OtherOutput
    end.
