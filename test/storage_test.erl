-module(storage_test).

-include_lib("eunit/include/eunit.hrl").

test_storage(Backend) ->
    Q0 = Backend:new(),
    ?assertEqual(0, Backend:len(Q0)),
    Q1 = Backend:append(1, Q0),
    Q2 = Backend:append(3, Q1),
    Q3 = Backend:append(5, Q2),
    ?assertEqual(3, Backend:len(Q3)),
    Q4 = Backend:append(5, Q3),
    ?assertEqual(4, Backend:len(Q4)),
    ?assertEqual({value, 1}, Backend:start_ts(Q4)),
    LBR = Backend:lower_bound(2, Q4),
    ?assertEqual({value, 3}, Backend:start_ts(LBR)),
    LBR2 = Backend:lower_bound(6, Q4),
    ?assertEqual(empty, Backend:start_ts(LBR2)).

tree_test() ->
    test_storage(imeter_ts_tree).

queue_test() ->
    test_storage(imeter_ts_queue).

ets_test() ->
    test_storage(imeter_ts_ets).
