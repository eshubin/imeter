%%%-------------------------------------------------------------------
%%% @author eugene
%%% @copyright (C) 2013, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 04. Dec 2013 2:06 PM
%%%-------------------------------------------------------------------
-module(imeter_SUITE).
-author("eugene").

-include_lib("common_test/include/ct.hrl").

%% API
-export([all/0]).
-export([init_per_suite/1]).
-export([end_per_suite/1]).
-export([handshake/1]).

all() ->
    [handshake].


init_per_suite(Config) ->
    imeter_utils:start_app_with_deps(imeter),
    Config.

handshake(_Config) ->
    {ok, Socket} = gen_tcp:connect("localhost", 8080,
        [binary, {active, false}, {packet, raw}]),
    ok = gen_tcp:send(Socket, [
        "GET /bullet HTTP/1.1\r\n"
        "Host: localhost\r\n"
        "Connection: Upgrade\r\n"
        "Upgrade: websocket\r\n"
        "Sec-WebSocket-Origin: http://localhost\r\n"
        "Sec-WebSocket-Version: 8\r\n"
        "Sec-WebSocket-Key: dGhlIHNhbXBsZSBub25jZQ==\r\n"
        "\r\n"]),
    {ok, Handshake} = gen_tcp:recv(Socket, 0, 6000),
    {ok, {http_response, {1, 1}, 101, "Switching Protocols"}, Rest}
        = erlang:decode_packet(http, Handshake, []),
    [Headers, <<>>] = websocket_headers(
        erlang:decode_packet(httph, Rest, []), []),
    {'Connection', "Upgrade"} = lists:keyfind('Connection', 1, Headers),
    {'Upgrade', "websocket"} = lists:keyfind('Upgrade', 1, Headers),
    {"sec-websocket-accept", "s3pPLMBiTxaQ9kYGzzhZRbK+xOo="}
        = lists:keyfind("sec-websocket-accept", 1, Headers),
%%     ok = gen_tcp:send(Socket, << 16#81, 16#85, 16#37, 16#fa, 16#21, 16#3d,
%%     16#7f, 16#9f, 16#4d, 16#51, 16#58 >>),
    {ok, << 1:1, 0:3, 1:4, 0:1, 5:7, "Hello" >>}
        = gen_tcp:recv(Socket, 0, 6000),
    {ok, << 1:1, 0:3, 1:4, 0:1, 14:7, "websocket_init" >>}
        = gen_tcp:recv(Socket, 0, 6000),
    {ok, << 1:1, 0:3, 1:4, 0:1, 16:7, "websocket_handle" >>}
        = gen_tcp:recv(Socket, 0, 6000),
    {ok, << 1:1, 0:3, 1:4, 0:1, 16:7, "websocket_handle" >>}
        = gen_tcp:recv(Socket, 0, 6000),
    {ok, << 1:1, 0:3, 1:4, 0:1, 16:7, "websocket_handle" >>}
        = gen_tcp:recv(Socket, 0, 6000),
    ok = gen_tcp:send(Socket, << 1:1, 0:3, 9:4, 1:1, 0:7, 0:32 >>), %% ping
    {ok, << 1:1, 0:3, 10:4, 0:8 >>} = gen_tcp:recv(Socket, 0, 6000), %% pong
    ok = gen_tcp:send(Socket, << 1:1, 0:3, 8:4, 1:1, 0:7, 0:32 >>), %% close
    {ok, << 1:1, 0:3, 8:4, 0:8 >>} = gen_tcp:recv(Socket, 0, 6000),
    {error, closed} = gen_tcp:recv(Socket, 0, 6000),
    ok.

end_per_suite(_Config) ->
    application:stop(imeter),
    ok.

websocket_headers({ok, http_eoh, Rest}, Acc) ->
    [Acc, Rest];
websocket_headers({ok, {http_header, _I, Key, _R, Value}, Rest}, Acc) ->
    F = fun(S) when is_atom(S) -> S; (S) -> string:to_lower(S) end,
    websocket_headers(erlang:decode_packet(httph, Rest, []),
        [{F(Key), Value}|Acc]).
