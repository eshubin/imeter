var wsUri = "ws://localhost:8080/bullet";
var output;
var upSpeedView;
var downSpeedView;
var state = "[0,0]";
var ws;
var sentNotifier;

function init() {
    upSpeedView = document.getElementById("upspeed");
    downSpeedView = document.getElementById("downspeed");
	output = document.getElementById("output");
    setInterval(updateSpeed,500);
	testWebSocket();
}

function testWebSocket() {
	ws = new WebSocket(wsUri);
	ws.onopen = function(evt) { onOpen(evt) };
	ws.onclose = function(evt) { onClose(evt) };
	ws.onmessage = function(evt) { onMessage(evt) };
	ws.onerror = function(evt) { onError(evt) };

	sentNotifier = new Worker("sent_notifier.js");
	sentNotifier.onmessage = handleSent;
}

function onOpen(evt) {
	writeToScreen("CONNECTED");
	scheduleSend();
}
function onClose(evt) { 
	sentNotifier.terminate();
	writeToScreen("DISCONNECTED");
} 
function onMessage(evt) {
    state = evt.data;
}
function onError(evt) {
    sentNotifier.terminate();
	writeToScreen('<span style="color: red;">ERROR:</span> ' + evt.data); 
}

function handleSent(e){
    ws.send("123456789012345678901234567890");
    scheduleSend();
}

function scheduleSend(){
    sentNotifier.postMessage();
}

function updateSpeed() {
    var arr = JSON.parse(state);
    downSpeedView.innerHTML = arr[0];
    upSpeedView.innerHTML = arr[1];
}
function writeToScreen(message) {
	var pre = document.createElement("p");
	pre.style.wordWrap = "break-word"; pre.innerHTML = message; output.appendChild(pre); 
}


window.addEventListener("load", init, false);